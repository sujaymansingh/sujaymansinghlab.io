FROM ruby:2.3

# https://github.com/jekyll/jekyll/issues/4268#issuecomment-167406574
# Install program to configure locales
RUN apt-get update
RUN apt-get install -y locales
RUN dpkg-reconfigure locales && \
  locale-gen C.UTF-8 && \
  /usr/sbin/update-locale LANG=C.UTF-8
# Install needed default locale for Makefly
RUN echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen && locale-gen
# Set default locale for the environment
ENV LC_ALL C.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8



VOLUME /app

RUN mkdir /dependencies
WORKDIR /dependencies
COPY ./Gemfile .
RUN /usr/local/bin/bundle install

WORKDIR /app
ENTRYPOINT ["/usr/local/bin/bundle", "exec", "jekyll"]
