

.PHONY: dev-dependencies
dev-dependencies:
	@docker build -t jekylltest .


.PHONY: dev-serve
dev-serve:
	@docker run -p 4000:4000 -v ${CURDIR}/:/app jekylltest serve --host=0.0.0.0
