---
layout: post
title: "Trust the dice (part 1)"
summary: "Limiting my coffee consumption using random numbers"
tags:
- coffee
- lifehacks
---


## I like coffee

In fact, I like it too much. I find it easy to slip into the habit of having
several cups of coffee a day.

This leaves me irritated and restless, it stops me from sleeping properly and
it leaves me 'needing' a coffee to fully wake up!

That's why, a while back I decided to cut down on my coffee consumption,
particularly at work.

One approach would be to limit myself to one coffee a day. That would solve
the restlessness and sleep issues (providing I don't have it too late!) but
I also wanted to avoid 'needing' a coffee.

As a result I decided that during the work week (Monday - Friday), I would
have coffees only on 3 days. (And of course no more than 1 on those days.)

Furthermore, I decided to randomly pick which days I could have coffee.


## Why random?

I could have just picked 3 specific days (e.g. Mon, Wed & Fri), but I wanted
to avoid 'getting used to' having coffee at a specific time or on a specific
day.

This is more of a mental thing (for example: to avoid putting things off until
Wednesday, when I know I'm having coffee) than physiological (I assume my
body doesn't have a 'week clock').

So I decided that every weekday morning, I'd hit the 'random' button on the
calculator on my phone (who knew they had random buttons?!) and if it was
less than 0.6, I could have a coffee!

(This means that some weeks I would have more than 3, some weeks fewer. But
that is great for me as it again avoids a schedule!)

But there was a bigger benefit of trusting the random number. Once I decided
that there were no exceptions, it was easy to beat the urge to break the rules
and have forbidden coffees.

It's like having a coach. Elite athletes benefit from coaches because they
spot and correct tiny problems in their technique. The rest of us benefit
because they shout at us and stop us from doing the wrong thing.

With a coach to make the decision for us, we don't need to expend as much
effort maintaining the discipline. In this case, the random number was the
coach. Anything above 0.6 meant no coffee, and it was pointless even
thinking about it.

This only works if you trust it completely. There can be no exceptions.
Didn't get much sleep last night? Doesn't matter. Big meeting later?
Doesn't matter. If you decide that you can, on occasion, overrule "Coach
Random" then the spell breaks.


## Did it work?

Yes! I managed to cut down coffee a lot. I slept better, I didn't get restless
but most importantly

- I didn't crave coffee
- I stopped relying on coffee (i.e. I could get through days without having coffee).

So much so that I'm going to try it in other areas.

But I decided to write a script to help me. I did this because:

- I wanted more complex choices (e.g. 3 options instead of two)
- I didn't want to have to add up the probabilities myself
- I switched to an Android phone and the calculator app doesn't have a random
  button


## Trust the dice

So I wrote a Python script [https://github.com/sujaymansingh/trustthedice](https://github.com/sujaymansingh/trustthedice).

I called it `trustthedice` because that sounds catchier than `trusttherng`.

You can then give it outcomes with individual probabilities, and let
it do the arithmetic and picking.

```
$ trustthedice random --outcome 'Coffee: 3 in 5' --outcome 'No coffee: 2 in 5'
Coffee
$ trustthedice random --outcome 'Coffee: 3 in 5' --outcome 'No coffee: 2 in 5'
Coffee
$ trustthedice random --outcome 'Coffee: 3 in 5' --outcome 'No coffee: 2 in 5'
No coffee
```

You don't have to specify all possible outcomes. You can specify one, and then
let everything else fall into an 'otherwise' category.
(Useful because then you don't need to ensure everything adds up to 1)

```
$ trustthedice random --outcome 'Coffee: 3 in 5' --otherwise 'No coffee'
Coffee
```

(I.e. `trustthedice` knows that the 'otherwise' option is whatever probability
is 'left over' from the main outcome.)

You can use it to specify more fine-grained options. E.g. you want a 1 in 10
chance of walking all the way to work, a 1 in 3 chance of walking half the way
and the other times you take the train:

```
$ trustthedice random --outcome 'Walk all the way: 1 in 10' --outcome 'Walk half the way: 1 in 3' --otherwise 'Take the train'
Walk half the way
```

Of course typing these out each time is tedious, so you can save them. (Just
`init` a project first: this creates a directory called `.trustthedice`.)

```
$ trustthedice init
$ trustthedice events save 'weekday-coffee' --outcome 'Coffee: 3 in 5' --otherwise 'No coffee'
$ trustthedice random --from-saved 'weekday-coffee'
Coffee
```


## What next?

I want to start adding this approach to other areas. E.g. going to the gym,
what I eat, forcing myself to set aside time for crucial but tedious tasks etc.

I also want to add the ability in the script to log what was generated.
(Partly for stats, but also so that there is a concrete record of what the
coach decided so that it can't be disputed :) )
